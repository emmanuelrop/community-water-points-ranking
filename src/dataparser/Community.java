/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataparser;
/**
 * Class to set and initialize variables
 * @author Emmanuel
 */
public class Community {
    private String communityName;
    private String waterFunction;
    
    public static final String KEY_URL = "https://raw.githubusercontent.com/onaio/ona-tech/master/data/water_points.json";
    public static final String KEY_COMMUNITY = "communities_villages";
    public static final String KEY_WATER_FUNCTIONING= "water_functioning";
    public static final String KEY_NUMBER_WATER_POINTS= "number_water_points";
    
    public static final String KEY_FUNCTIONAL_YES= "yes";    
    public static final String KEY_NUMBER_FUNCTIONAL= "number_functional";
    public static final String KEY_PERCENTAGE= "percentage";
    public static final String KEY_RANK= "community_rank";

    public static String getKEY_COMMUNITY() {
        return KEY_COMMUNITY;
    }
    
    

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getWaterFunction() {
        return waterFunction;
    }

    public void setWaterFunction(String waterFunction) {
        this.waterFunction = waterFunction;
    }
    
    
}
