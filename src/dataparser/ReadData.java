/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataparser;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.FileReader;
/**
 *
 * @author Upande
 */
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 * Class to read through a json data structure from a url and write specific
 * data to file
 *
 * @author Emmanuel
 */
public class ReadData {

    private static String key;
    private List communityDataList = new ArrayList();
    private final List communityStatusList = new ArrayList();
    private List containerList = new ArrayList();
    private List communityRankList = new ArrayList();
    HashSet communityDataSet = new HashSet();
    HashSet statusDataSet = new HashSet();
    HashMap<String, Float> rankMap = new HashMap<>();
    HashMap<String, String> map = new HashMap<>();
    HashMap<String, Integer> dataMap = new HashMap<>();
    HashMap<String, String> resultMap = new HashMap<>();
    HashMap<String, Object> containerMap = new HashMap<>();
    String[] dataArray = new String[3];
    int yesCounter = 0;
    int counter = 0;
    int communitySize = 0;
    int cummunityItemIndex = 0;
    private String community_name;
    private String isFunctioning;
    int index = 0;
    JsonWriter writer;
    HashMap<String, String>[] dataArr;
    Gson gson = new Gson();

    public static void main(String[] args) {
        String communityData = new ReadData().calculate(Community.KEY_URL);
        System.out.println(communityData);
    }

    /**
     * Get the url, stream the json data and uses Maps to update data
     *
     * @param sUrl String value
     * @return String with json data structure.
     */
    private String calculate(String sUrl) {
        map = streamJsonData(sUrl);
        HashMap<String, Float> mapToSort = new HashMap<>();

        float percentage;
        int total;
        int yesCount;

        /**
         * mapToSort object is related to map object. map at this point
         * contains community name as key and counts of functional water points as value. 
         * The value will be changed by percentage for easy sorting
         */
        for (String mapkey : map.keySet()) {
            total = Collections.frequency(communityDataList, mapkey);
            yesCount = Integer.parseInt(map.get(mapkey));
            percentage = Math.round((float) yesCount / total * 100);
            mapToSort.put(mapkey, percentage);
            /**
             * resultMap object holds data for every community and is added to
             * another object (containerMap) which holds all the resultMap
             * objects. At this point, we introduce the other parameters a community to the object
             * 
             * containerMap has its key as the community name for easy
             * update of these data.
             */
            resultMap = new HashMap<>();
            resultMap.put(Community.KEY_NUMBER_FUNCTIONAL, String.valueOf(yesCount));
            resultMap.put(Community.KEY_NUMBER_WATER_POINTS, String.valueOf(total));
            resultMap.put(Community.KEY_PERCENTAGE, String.valueOf(percentage));
            containerMap.put(mapkey, resultMap);

        }
        HashMap<String, String> sortedMap = getSortedMap(mapToSort);
        return writeToJson(sortedMap, containerMap);

    }

    /**
     * Method to stream through json data froma url and get specific data sets
     *
     * @param sUrl Link url to get the json data
     * @return HashMap with only required data
     */
    public HashMap streamJsonData(String sUrl) {
        try {
            // Connect to the URL using java's native library
            System.out.println("Reading from url");
            URL url = new URL(sUrl);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();
            JsonReader reader = new JsonReader(new InputStreamReader((InputStream) request.getContent()));
//            JsonReader reader = new JsonReader(new FileReader(sUrl));
            try {
                reader.beginArray();
                while (reader.hasNext()) {
                    reader.beginObject();
                    while (reader.hasNext()) {
                        key = reader.nextName();
                        switch (key) {
                            /**
                             * map object is used to hold the community name and
                             * the counts a functioning water point This object
                             * will later have its values (counts) changed to
                             * the percentage preparing it for ranking
                             */
                            case Community.KEY_COMMUNITY:

                                community_name = reader.nextString();

                                communityDataList.add(community_name);
                                if (!communityDataSet.contains(community_name)) { //New entry                                    
                                    communityDataSet.add(community_name);
                                    map.put(community_name, "0");
                                    communitySize++;
                                } else {
                                    yesCounter += Integer.parseInt(map.get(community_name));// This is done to solve a case where data of same community are not serial                                   
                                    map.put(community_name, String.valueOf(yesCounter));
                                }

                                break;
                            case Community.KEY_WATER_FUNCTIONING:
                                isFunctioning = reader.nextString();
                                communityStatusList.add(isFunctioning);
                                if (isFunctioning.equals(Community.KEY_FUNCTIONAL_YES)) {
                                    yesCounter = Integer.parseInt(map.get(community_name));
                                    yesCounter++;
                                    map.put(community_name, String.valueOf(yesCounter));
                                }
                                break;
                            default:
                                reader.skipValue();
                        }

                    }
                    index++;
                    yesCounter = 0;
                    counter = 0;
                    reader.endObject();
                }
                reader.endArray();
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadData.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                reader.close();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ReadData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReadData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    /**
     * Method to return the rank of a community.
     *
     * @param sortedMap A hashmap already sorted ready for ranking based on
     * percentage
     * @param name Community name to be ranked
     * @return the rank
     */
    public int getRank(HashMap<String, String> sortedMap, String name) {
        int rank = 1;
        try {
            for (String mapkey : sortedMap.keySet()) {
                if (mapkey.equals(name)) {
                    return rank++;
                }
                rank++;
            }
        } catch (NullPointerException ex) {
            Logger.getLogger(ReadData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    /**
     * Method to Commit the final json data structure to a file. Maps are used
     * for easy updates and avoidance of duplications The file path can too be a
     * parameter
     *
     * @param sortedMap A hashmap already sorted ready for ranking based on
     * percentage
     * @param containerMap Takes in Hash containing nested HashMaps of community
     * data sets
     * @param String Json string containing sets of hashmap that have data for
     * each community.
     * @return
     */
    public String writeToJson(HashMap<String, String> sortedMap, HashMap<String, Object> containerMap) {
//        map = (HashMap<String, Integer>) getSortedMap(mapToSort);
//System.out.println(sortedMap+"_"+containerMap);
        try {
            HashMap<String, String> displayMap = new HashMap<>();
            for (String mapkey : sortedMap.keySet()) {
                displayMap = (HashMap<String, String>) containerMap.get(mapkey);
                int rank = getRank(sortedMap, String.valueOf(mapkey));
                displayMap.put(Community.KEY_RANK, String.valueOf(rank));
                rank++;
            }

        } catch (Exception ex) {
            Logger.getLogger(ReadData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return gson.toJson(containerMap);
    }

    /**
     * Returns a sorted hashmap based on percentage of water functioning.
     *
     * @param mapToSort Unsorted Map object.
     * @return Hashmap with sorted values descending
     */

    public HashMap getSortedMap(HashMap mapToSort) {
        MapUtil m = new MapUtil();
        Map newMap = m.sortByValue(mapToSort);
        return (HashMap) newMap;
    }
}
