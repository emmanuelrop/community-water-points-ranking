/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataparser;

import java.net.MalformedURLException;
import java.util.HashMap;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Upande
 */
public class ReadDataTest {

    /**
     * Test of getSortedMap method, of class ReadData.
     */
    @Test(expected = NullPointerException.class)
    public void testGetSortedMap() {
        HashMap mapToSort = null;
        ReadData instance = new ReadData();
        assertNotNull(instance.getSortedMap(mapToSort));
    }

    @Test
    public void testGetSortedMap2() {
        HashMap mapToSort = new HashMap();
        mapToSort.put("C", 50);
        mapToSort.put("A", 56);
        mapToSort.put("B", 111);
        ReadData instance = new ReadData();
        HashMap expResult = new HashMap();
        expResult.put("B", 111);
        expResult.put("A", 56);
        expResult.put("C", 50);

        HashMap result = instance.getSortedMap(mapToSort);
        assertThat(result, is(expResult));
    }

    @Test(expected = ClassCastException.class)
    public void testGetSortedMap3() {
        HashMap mapToSort = new HashMap();
        mapToSort.put("C", "dad");
        mapToSort.put("A", 56);
        mapToSort.put("B", 111);
        ReadData instance = new ReadData();
        HashMap expResult = new HashMap();
        expResult.put("B", 111);
        expResult.put("A", 56);
        expResult.put("C", 50);

        HashMap result = instance.getSortedMap(mapToSort);
        assertThat(result, is(expResult));
    }

    @Test
    public void testGetSortedMap4() {
        HashMap mapToSort = new HashMap();
        mapToSort.put("C", 21);
        mapToSort.put("A", 2);
        mapToSort.put("B", 11);
        ReadData instance = new ReadData();
        HashMap expResult = new HashMap();
        expResult.put("C", 21);
        expResult.put("B", 11);
        expResult.put("A", 2);

        HashMap result = instance.getSortedMap(mapToSort);
        assertThat(result, is(expResult));
    }


    @Test 
    public void testWriteToFile(){
        HashMap sortedMap = new HashMap();
        sortedMap.put("X", 86);
        sortedMap.put("Z", 67);
        sortedMap.put("Y", 50);
        
        HashMap<String, Object> container_map = new HashMap();
        HashMap <String, Object> map2 = new HashMap();
        map2.put(Community.KEY_NUMBER_FUNCTIONAL, "12");
        map2.put(Community.KEY_NUMBER_WATER_POINTS, "14");
        map2.put(Community.KEY_PERCENTAGE,"86");
        container_map.put("X", map2);
        
        HashMap <String, Object> map3 = new HashMap();
        map2.put(Community.KEY_NUMBER_FUNCTIONAL, "2");
        map2.put(Community.KEY_NUMBER_WATER_POINTS, "4");
        map2.put(Community.KEY_PERCENTAGE,"50");
        container_map.put("Y", map3);
        
        HashMap map4 = new HashMap();
        map2.put(Community.KEY_NUMBER_FUNCTIONAL, "2");
        map2.put(Community.KEY_NUMBER_WATER_POINTS, "3");
        map2.put(Community.KEY_PERCENTAGE,"67");
        container_map.put("Z", map4);
        ReadData instance = new ReadData();
        
        assertNotNull(instance.writeToJson(sortedMap, container_map));
    }
    
    
      @Test
    public void testGetRank_correctness() {
         HashMap sortedMap = new HashMap();
        sortedMap.put("X", 86);
        sortedMap.put("Z", 67);
        sortedMap.put("Y", 50);
        ReadData instance = new ReadData();
        int expResult = 3;
        int result = instance.getRank(sortedMap, "Z");
        assertEquals(expResult, result);
    }
    
     
      @Test
    public void testGetRank_null() {
         HashMap sortedMap = new HashMap();
        sortedMap.put("X", 86);
        sortedMap.put("Z", 67);
        sortedMap.put("Y", 50);
        ReadData instance = new ReadData();
        int expResult = 3;
        int result = instance.getRank(sortedMap, "Z");
        assertNotNull(sortedMap.get("Z"));
    }

     @Test  
    public void testStreamJsonData(){
        ReadData instance = new ReadData();
       assertNotNull(instance.streamJsonData(Community.KEY_URL));
    }
    
}
