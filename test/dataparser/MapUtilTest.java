/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataparser;

import java.util.HashMap;
import junit.framework.AssertionFailedError;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class MapUtilTest {

    /**
     * Test of sortByValue method, of class MapUtil.
     */
    @Test (expected = AssertionError.class)
    public void testSortByValue_Incorrect() {
         HashMap mapToSort = new HashMap();
        mapToSort.put("C", 1);
        mapToSort.put("A", 2);
        mapToSort.put("B", 11);
        ReadData instance = new ReadData();
        HashMap expResult = new HashMap();
        expResult.put("C", 21);
        expResult.put("B", 11);
        expResult.put("A", 2);
        HashMap result = MapUtil.sortByValue(mapToSort);
        assertEquals(expResult, result);
    }
     @Test 
    public void testSortByValue_Correct() {
         HashMap mapToSort = new HashMap();
        mapToSort.put("C", 21);
        mapToSort.put("A", 2);
        mapToSort.put("B", 11);
        ReadData instance = new ReadData();
        HashMap expResult = new HashMap();
        expResult.put("C", 21);
        expResult.put("B", 11);
        expResult.put("A", 2);
        HashMap result = MapUtil.sortByValue(mapToSort);
        assertEquals(expResult, result);
    }
    
}
